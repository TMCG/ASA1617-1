#include <stdio.h>
#include <stdlib.h>

/*Data Structures*/
struct node{
  int number;
  struct node* next;
};

typedef struct node* link;

/*Global Variables*/
link *graph;
int numComponents, coherentFlag, suficientFlag, dTime;
int* visited; /*0,1 e 2 corresponde respetivamente a nao descoberto, descoberto e fechado*/
int* topological;
int remainingEdges; /* para a ordem topologica */

/*Prototypes*/
void createGraph();
link createNode(int photoNumber);
void addNode(int node, int photoNumber);
void evaluate();
void DFS();
void DFSVisit(int i);
void hamiltonianPath();
int directEdge(int u, int v);
void printOutput();
void freeGraph();
void freeMemory();

int main(int argc, char** argv){
  createGraph();
  evaluate();
  printOutput();
  freeMemory();

  return 0;
}

void freeMemory(){
  free(visited);
  free(topological);
  freeGraph();
}

void freeGraph(){
  int i=0;
  link linkDel = NULL;
  link linkAux = NULL;
  for(i = 0; i < numComponents; i++){
    linkAux = graph[i];
    while(linkAux != NULL){
      linkDel = linkAux;
      linkAux = linkAux-> next;
      free(linkDel);
    }
  }
  free(graph);
}

void createGraph(){
  int components = 0, edges = 0, photoNumber = 0, node = 0, i = 0;
  if (scanf("%d %d", &components, &edges) < 0){
    perror("Error getting input from stdin.");
  }
  numComponents = components;
  graph = (link*) malloc(sizeof(link) * components);

  for(i=0; i<components;i++){
    graph[i] = NULL;
  }

  for(i=0; i<edges; i++){
    if (scanf("%d %d", &node, &photoNumber) < 0){
      perror("Error getting input from stdin.");
    }
    addNode(node, photoNumber);
  }
}

void addNode(int node, int photoNumber){
  link newNode = createNode(photoNumber);
  newNode->next = graph[node-1];
  graph[node-1] = newNode;
}

link createNode(int photoNumber){
  link newNode = (link) malloc(sizeof(struct node));
  newNode->number = photoNumber;
  newNode->next = NULL;
  return newNode;
}

void evaluate(){
  DFS();
  hamiltonianPath();
}

void DFS(){
  int i = 0;
  dTime = 0;
  coherentFlag = 0;
  remainingEdges = numComponents;
  visited = (int*) malloc(sizeof(int) * numComponents);
  topological = (int*) malloc(sizeof(int) * numComponents);

  for(i = 0; i < numComponents; i++){
    visited[i] = 0;
    topological[i] = 0;
  }

  for(i = 0; i < numComponents; i++){
    if (!visited[i]){
      DFSVisit(i);
    }
  }
}

void DFSVisit(int v){
  link currentLink = graph[v];
  visited[v] = 1;
  dTime++;

  while(currentLink != NULL){
    if (!visited[currentLink->number-1]){
      DFSVisit(currentLink->number-1);
    }
    else if(visited[currentLink->number-1] == 1){ /* se encontrar un vertice ja visitado mas nao fechado entao existe um ciclo */
      coherentFlag = 1;                             /* logo nao e' coerente */
    }
    currentLink = currentLink->next;
  }

  visited[v] = 2;
  remainingEdges--;                                 /* introduzir por ordem topologica */
  topological[remainingEdges] = v+1;
  dTime++;
}

void hamiltonianPath(){
  int i = 0;
  int u,v;
  suficientFlag = 0;

  for(i = 0; i < numComponents - 1; i++){
    u = topological[i];
    v = topological[i + 1];

    if(!directEdge(u,v)){
      suficientFlag = 1;
      break;
    }
  }
}

int directEdge(int u, int v){
  link linkAux = graph[u-1];
  while(linkAux != NULL){
    if(linkAux->number == v){
      return 1;
    }
    linkAux = linkAux->next;
  }
  return 0;
}

void printOutput(){
  int i = 0;

  if(coherentFlag){
    printf("Incoerente\n");
  }
  else if(suficientFlag){
    printf("Insuficiente\n");
  }
  else{
    for(i = 0; i < numComponents - 1; i++){
      printf("%d ",topological[i]);
    }
    printf("%d\n",topological[numComponents-1]);
  }
}
